#include "env.h"

void drawBlock(SDL_Renderer* renderer, int x, int y);
void drawGrid(SDL_Renderer* renderer);
void drawScore(struct GameEnv*);
void drawTetro(SDL_Renderer* renderer,int x,int y);
void drawAll(struct GameEnv*);
