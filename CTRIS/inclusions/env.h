#ifndef ENV_H
#define ENV_H

#include <SDL2/SDL.h>
#include "gamestate.h"
#include "audio.h"

struct SDLenv {
  SDL_Window* window;
  SDL_Renderer* renderer;
  SDL_Event event;
  int fps;
  int frame_start;
};

struct GameEnv {
  struct SDLenv SDLenv;
  struct State gamestate;
  struct Sounds gamesounds;
};
  
struct SDLenv setSdlEnv();

struct GameEnv setGameEnv();

void frameAlignment(struct GameEnv*);

void render(SDL_Renderer*);

void quit(struct GameEnv);

#endif
