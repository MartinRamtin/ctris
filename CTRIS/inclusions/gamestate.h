#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "stdbool.h"

struct GameEnv;

struct State {
  bool game_over;
  int gravity;
  int gravity_countdown;
  int grid[10][20];
  int tetro_x;
  int tetro_y;
  bool space;
  bool left;
  bool right;
  int already_left;
  int already_right;
  bool soft_drop;
  bool hard_drop;
  bool already_hard_drop;
  int score;
  bool running;
};

struct State setState();

void setGrid(struct State*, bool);

void printGrid(struct State*);

void engraveTetro(struct State*);

bool findLine(struct State*, int*);

void destroyLines(struct State*, int);

void checkLines(struct State*);

void speed(struct GameEnv*, int);

void gravity(struct GameEnv*);

void gameOver(struct GameEnv*);

void update(struct GameEnv*);

#endif
