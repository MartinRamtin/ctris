#ifndef MOVE_H
#define MOVE_H

#include "audio.h"
#include "env.h"

enum Axe { HORIZONTAL, VERTICAL };

enum Direction { LEFT, RIGHT };

void moveTetro(struct GameEnv*, enum Direction);

void initLateralMove(struct GameEnv*, int*, enum Direction);

void lateralMove(struct GameEnv*);

// bool for hard drop
void drop(struct GameEnv*, bool);

#endif
