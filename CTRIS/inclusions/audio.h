#ifndef AUDIO_H
#define AUDIO_H

#include <SDL2/SDL_mixer.h>

struct Sounds {
  Mix_Music* music;
  Mix_Chunk* move;
  Mix_Chunk* collision;
  Mix_Chunk* rotation;
};

struct Sounds SetAudio();

#endif