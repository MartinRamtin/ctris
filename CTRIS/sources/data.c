#include "data.h"

const int GRID_WIDTH = 10;
const int GRID_HEIGHT = 20;
const int BLOCK_SIZE = 30;
const int SCREEN_W = 16 * BLOCK_SIZE;
const int SCREEN_H = 600;
const char TITLE[] =
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~ CTRIS ~~~~~~~~~~~~~~~~~~~~~~~~~~~";
const char MUSIC[] = "y_sounds/music.wav";
const char MOVE[] = "y_sounds/move.wav";
const char COLLISION[] = "y_sounds/collision.wav";
const char ROTATION[] = "y_sounds/rotation.wav";
const int FPS = 60;
