#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "data.h"
#include "drawing.h"

void drawBlock(SDL_Renderer* renderer, int x, int y) {
  SDL_Rect blockRect = {x, y, BLOCK_SIZE, BLOCK_SIZE};
  SDL_SetRenderDrawColor(renderer, 20, 20, 20, 255);
  SDL_RenderFillRect(renderer, &blockRect);
}

void drawScore(struct GameEnv* game_env) {
  SDL_SetRenderDrawColor(game_env->SDLenv.renderer, 20, 20, 20, 255);
  SDL_Rect scoreRect = {10, 20, 70, 20};
  SDL_RenderFillRect(game_env->SDLenv.renderer, &scoreRect);
  SDL_SetRenderDrawColor(game_env->SDLenv.renderer, 0, 0, 0, 255);
  char scoreText[50];
  snprintf(scoreText, sizeof(scoreText), "%d", game_env->gamestate.score);
  TTF_Font* font = TTF_OpenFont("y_fonts/Coffee_Extra.ttf", 25);
  SDL_Color textColor = {107, 142, 35};
  SDL_Surface* textSurface = TTF_RenderText_Solid(font, scoreText, textColor);
  SDL_Texture* textTexture =
      SDL_CreateTextureFromSurface(game_env->SDLenv.renderer, textSurface);
  int texW = 0;
  int texH = 0;
  SDL_QueryTexture(textTexture, NULL, NULL, &texW, &texH);
  SDL_Rect textRect = {12, 12, texW, texH};
  SDL_RenderCopy(game_env->SDLenv.renderer, textTexture, NULL, &textRect);
  SDL_DestroyTexture(textTexture);
  SDL_FreeSurface(textSurface);
  TTF_CloseFont(font);
}

void drawGameOver(struct GameEnv* game_env) {
  SDL_SetRenderDrawColor(game_env->SDLenv.renderer, 0, 0, 0, 255);
  TTF_Font* font = TTF_OpenFont("y_fonts/Coffee_Extra.ttf", 25);
  SDL_Color textColor = {205, 92, 92};
  SDL_Surface* textSurface =
      TTF_RenderText_Solid(font, "Press SPACE to play", textColor);
  SDL_Texture* textTexture =
      SDL_CreateTextureFromSurface(game_env->SDLenv.renderer, textSurface);
  int texW = 0;
  int texH = 0;
  SDL_QueryTexture(textTexture, NULL, NULL, &texW, &texH);
  SDL_Rect textRect = {BLOCK_SIZE * 4, BLOCK_SIZE * 8, texW, texH};
  SDL_RenderCopy(game_env->SDLenv.renderer, textTexture, NULL, &textRect);
  SDL_DestroyTexture(textTexture);
  SDL_FreeSurface(textSurface);
  TTF_CloseFont(font);
}

void drawGrid(SDL_Renderer* renderer) {
  for (int i = 3; i < GRID_WIDTH + 3; i++) {
    for (int j = 0; j < GRID_HEIGHT; j++) {
      drawBlock(renderer, i * BLOCK_SIZE, j * BLOCK_SIZE);
    }
  }
  SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);

  for (int i = 1; i < GRID_HEIGHT; i++) {
    SDL_RenderDrawLine(renderer, 3 * BLOCK_SIZE, i * BLOCK_SIZE,
                       (GRID_WIDTH + 3) * BLOCK_SIZE, i * BLOCK_SIZE);
  }
  for (int i = 3; i < GRID_WIDTH + 4; i++) {
    SDL_RenderDrawLine(renderer, i * BLOCK_SIZE, 0, i * BLOCK_SIZE,
                       GRID_HEIGHT * BLOCK_SIZE);
  }
}

void drawTetro(SDL_Renderer* renderer, int x, int y) {
  SDL_Rect rect;
  rect.x = (x)*BLOCK_SIZE + 3 * BLOCK_SIZE + 3;
  rect.y = (19 - y) * BLOCK_SIZE + 3;
  rect.w = BLOCK_SIZE - 5;
  rect.h = BLOCK_SIZE - 5;
  SDL_SetRenderDrawColor(renderer, 135, 206, 235, 255);
  SDL_RenderFillRect(renderer, &rect);
}

void drawAll(struct GameEnv* game_env) {
  SDL_SetRenderDrawColor(game_env->SDLenv.renderer, 5, 5, 5, 255);
  SDL_RenderClear(game_env->SDLenv.renderer);
  drawGrid(game_env->SDLenv.renderer);
  drawScore(game_env);
  if (game_env->gamestate.game_over) {
    drawGameOver(game_env);
  } else {
    for (int y = 0; y < 20; ++y) {
      for (int x = 0; x < 10; ++x) {
        if (game_env->gamestate.grid[x][y]) {
          drawTetro(game_env->SDLenv.renderer, x, y);
        }
      }
    }
    drawTetro(game_env->SDLenv.renderer, game_env->gamestate.tetro_x,
              game_env->gamestate.tetro_y);
  }
}