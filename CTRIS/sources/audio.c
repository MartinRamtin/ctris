#include "audio.h"
#include "data.h"

struct Sounds SetAudio() {
  struct Sounds game_sound;
  Mix_OpenAudio(96000, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024);
  game_sound.music = Mix_LoadMUS(MUSIC);
  Mix_AllocateChannels(3);
  game_sound.move = Mix_LoadWAV(MOVE);
  game_sound.collision = Mix_LoadWAV(COLLISION);
  game_sound.rotation = Mix_LoadWAV(ROTATION);
  Mix_Volume(0, 5);
  Mix_PlayMusic(game_sound.music, -1);
  return game_sound;
}
