#include "move.h"

bool nextBlockIsAllowed(struct State* game_state) {
  int x = game_state->tetro_x;
  int y = game_state->tetro_y;
  int tetro = game_state->grid[x][y - 1];
  if (y <= 0 || tetro) {
    return false;
  }
  return true;
}

bool horizontalTetroCollision(struct GameEnv* game_env, int direction) {
  return (game_env->gamestate.grid[game_env->gamestate.tetro_x + direction]
                                  [game_env->gamestate.tetro_y]);
}

void groundCollision(struct GameEnv* game_env) {
  Mix_PlayChannel(1, game_env->gamesounds.collision, 0);
  game_env->gamestate.tetro_y = 19;
  game_env->gamestate.tetro_x = 4;
}

void moveTetro(struct GameEnv* game_env, enum Direction direction) {
  switch (direction) {
    case LEFT:
      if (!horizontalTetroCollision(game_env, -1)) {
        if (game_env->gamestate.tetro_x > 0) {
          game_env->gamestate.tetro_x = game_env->gamestate.tetro_x - 1;
          Mix_PlayChannel(0, game_env->gamesounds.move, 0);
        }
      }
      break;
    case RIGHT:
      if (!horizontalTetroCollision(game_env, 1)) {
        if (game_env->gamestate.tetro_x < 9) {
          game_env->gamestate.tetro_x = game_env->gamestate.tetro_x + 1;
          Mix_PlayChannel(0, game_env->gamesounds.move, 0);
        }
      }
      break;
  }
}

void initLateralMove(struct GameEnv* game_env,
                     int* already_done,
                     enum Direction direction) {
  if (!*already_done || *already_done > 5) {
    moveTetro(game_env, direction);
  }
  *already_done += 1;
}

void lateralMove(struct GameEnv* game_env) {
  if (game_env->gamestate.left) {
    enum Direction direction = LEFT;
    initLateralMove(game_env, &game_env->gamestate.already_left, direction);
  }
  if (game_env->gamestate.right) {
    enum Direction direction = RIGHT;
    initLateralMove(game_env, &game_env->gamestate.already_right, direction);
  }
}

void drop(struct GameEnv* game_env, bool hard_drop) {
  if (nextBlockIsAllowed(&game_env->gamestate)) {
    game_env->gamestate.tetro_y = game_env->gamestate.tetro_y - 1;
    if (hard_drop) {
      drop(game_env, hard_drop);
    }
  } else {
    engraveTetro(&game_env->gamestate);
    groundCollision(game_env);
  }
}
