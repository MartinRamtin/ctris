#include "env.h"
#include "inputs.h"

void processInput(struct GameEnv* game_env) {
  while (SDL_PollEvent(&game_env->SDLenv.event)) {
    switch (game_env->SDLenv.event.type) {
      case SDL_KEYDOWN:
        switch (game_env->SDLenv.event.key.keysym.sym) {
          case SDLK_SPACE:
            game_env->gamestate.space = true;
            break;
          case SDLK_LEFT:
            game_env->gamestate.left = true;
            break;
          case SDLK_RIGHT:
            game_env->gamestate.right = true;
            break;
          case SDLK_DOWN:
            game_env->gamestate.soft_drop = true;
            break;
          case SDLK_UP:
            if (game_env->gamestate.already_hard_drop) {
              game_env->gamestate.hard_drop = false;
            } else {
              game_env->gamestate.hard_drop = true;
              game_env->gamestate.already_hard_drop = true;
            }
            break;
        }
        break;
      case SDL_KEYUP:
        switch (game_env->SDLenv.event.key.keysym.sym) {
          case SDLK_LEFT:
            game_env->gamestate.left = false;
            game_env->gamestate.already_left = 0;
            break;
          case SDLK_RIGHT:
            game_env->gamestate.right = false;
            game_env->gamestate.already_right = 0;
            break;
          case SDLK_DOWN:
            game_env->gamestate.soft_drop = false;
            break;
          case SDLK_UP:
            game_env->gamestate.hard_drop = false;
            game_env->gamestate.already_hard_drop = false;
            break;
          case SDLK_SPACE:
            game_env->gamestate.space = false;
            break;
        }
        break;
      case SDL_QUIT:
        game_env->gamestate.running = false;
        break;
    }
  }
}
