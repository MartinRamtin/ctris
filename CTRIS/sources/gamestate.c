#include "data.h"
#include "drawing.h"
#include "env.h"
#include "gamestate.h"
#include "move.h"

struct State setState() {
  struct State game_state;
  game_state.game_over = true;
  game_state.gravity = 30;
  game_state.gravity_countdown = game_state.gravity;
  game_state.tetro_x = 4;
  game_state.tetro_y = GRID_HEIGHT - 1;
  game_state.space = false;
  game_state.left = false;
  game_state.right = false;
  game_state.already_left = 0;
  game_state.already_right = 0;
  game_state.soft_drop = false;
  game_state.hard_drop = false;
  game_state.already_hard_drop = false;
  game_state.score = 0;
  game_state.running = true;
  return game_state;
}

void setGrid(struct State* game_state, bool set_tetro) {
  for (int y = 0; y < 20; ++y) {
    for (int x = 0; x < 10; ++x) {
      game_state->grid[x][y] = set_tetro;
    }
  }
}

void printGrid(struct State* game_state) {
  for (int y = GRID_HEIGHT - 1; y >= 0; --y) {
    for (int x = 0; x < 10; ++x) {
      printf("%d ", game_state->grid[x][y]);
    }
    printf("\n");
  }
  printf("\n");
}

void engraveTetro(struct State* game_state) {
  game_state->grid[game_state->tetro_x][game_state->tetro_y] = true;
}

bool findLine(struct State* game_state, int* line_number) {
  for (int y = GRID_HEIGHT - 1; y >= 0; --y) {
    bool line_found = true;
    for (int x = 0; x < 10; ++x) {
      if (!game_state->grid[x][y]) {
        line_found = false;
        break;
      }
    }
    if (line_found) {
      *line_number = y;
      return true;
    }
  }
  return false;
}

void destroyLines(struct State* game_state, int line_number) {
  for (int current_line = line_number; current_line < GRID_HEIGHT - 1;
       ++current_line) {
    for (int i = 0; i < 10; ++i) {
      game_state->grid[i][current_line] = game_state->grid[i][current_line + 1];
    }
    checkLines(game_state);
  }
}

void checkLines(struct State* game_state) {
  int line_number;
  if (findLine(game_state, &line_number)) {
    destroyLines(game_state, line_number);
    game_state->score += 1;
  }
}

void speed(struct GameEnv* game_env, int speed) {
  game_env->gamestate.gravity_countdown -= speed;
}

void gravity(struct GameEnv* game_env) {
  if (game_env->gamestate.hard_drop) {
    game_env->gamestate.hard_drop = false;
    game_env->gamestate.gravity_countdown = game_env->gamestate.gravity;
    drop(game_env, true);
  } else {
    if (game_env->gamestate.soft_drop) {
      speed(game_env, 20);
    } else {
      speed(game_env, 1);
    }
    if (game_env->gamestate.gravity_countdown <= 0) {
      game_env->gamestate.gravity_countdown = game_env->gamestate.gravity;
      drop(game_env, false);
    }
  }
}

void gameOver(struct GameEnv* game_env) {
  if (game_env->gamestate.grid[4][GRID_HEIGHT - 1]) {
    game_env->gamestate.game_over = true;
    setGrid(&game_env->gamestate, false);
    game_env->gamestate.tetro_y = GRID_HEIGHT - 1;
    game_env->gamestate.tetro_x = 4;
  }
}

void update(struct GameEnv* game_env) {
  gameOver(game_env);
  if (game_env->gamestate.game_over) {
    if (!game_env->gamestate.space) {
      drawAll(game_env);
    } else {
      game_env->gamestate.game_over = false;
      game_env->gamestate.score = 0;
    }
  } else {
    lateralMove(game_env);
    gravity(game_env);
    checkLines(&game_env->gamestate);
    drawAll(game_env);
  }
}
