#include <SDL2/SDL_ttf.h>
#include "data.h"
#include "env.h"

struct SDLenv setSdlEnv() {
  SDL_Init(SDL_INIT_VIDEO);
  TTF_Init();
  struct SDLenv env;
  SDL_CreateWindowAndRenderer(SCREEN_W, SCREEN_H, 0, &env.window,
                              &env.renderer);
  SDL_SetWindowTitle(env.window, TITLE);
  env.fps = 1000 / FPS;
  env.frame_start = 0;
  return env;
}

struct GameEnv setGameEnv() {
  struct GameEnv game_env;
  struct SDLenv SDLenv = setSdlEnv();
  struct State game_state = setState();
  struct Sounds game_sounds = SetAudio();
  setGrid(&game_state, 0);
  game_env.SDLenv = SDLenv;
  game_env.gamestate = game_state;
  game_env.gamesounds = game_sounds;
  return game_env;
}

void frameAlignment(struct GameEnv* game_env) {
  int elapsed = SDL_GetTicks() - game_env->SDLenv.frame_start;
  int remaining_time = game_env->SDLenv.fps - elapsed;
  SDL_Delay(remaining_time);
}

void render(SDL_Renderer* renderer) {
  SDL_RenderPresent(renderer);
}

void quit(struct GameEnv game_env) {
  SDL_DestroyRenderer(game_env.SDLenv.renderer);
  SDL_DestroyWindow(game_env.SDLenv.window);
  TTF_Quit();
  SDL_Quit();
}
