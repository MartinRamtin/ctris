#include "env.h"
#include "inputs.h"

int main() {
  struct GameEnv game_env = setGameEnv();

  while (game_env.gamestate.running) {
    game_env.SDLenv.frame_start = SDL_GetTicks();
    processInput(&game_env);
    update(&game_env);
    render(game_env.SDLenv.renderer);
    frameAlignment(&game_env);
  }
  quit(game_env);
  return 0;
}
